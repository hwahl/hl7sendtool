#!/usr/bin/env python3

import argparse
from lib.hl7 import HL7Msg


def main():
    parser = argparse.ArgumentParser(description='Sends HL7-Message to specified host and port.')
    parser.add_argument('--host', default='127.0.0.1', help='xnat hostname')
    parser.add_argument('--port', default=65432)
    parser.add_argument('--dob', default="2009-12-12")
    parser.add_argument('--project', default="666")
    parser.add_argument('--subject', default="Bernd")
    parser.add_argument('--session', default="fmri")
    parser.add_argument('--studydate', default="2019-12-01")

    args = parser.parse_args()

    HL7 = HL7Msg(project_id=args.project, subject_id=args.subject, subject_dob=args.dob,
                           session_id=args.session, study_date=args.studydate, host=args.host, port=args.port)
    try:
        HL7.sendHL7()
    except:
        pass

if __name__ == '__main__':
    main()
