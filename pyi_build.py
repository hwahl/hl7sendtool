#!/usr/bin/env python3

import PyInstaller
import os

# my spec file in "dev\config" dir
workdir = os.getcwd()
win_spec = os.path.join(workdir, 'hl7st_win.spec')

# define the "dev\dist" and "dev\build" dirs
devdir = os.getcwd()
distdir = os.path.join(devdir, 'dist')
builddir = os.path.join(devdir, 'build')

# call pyinstaller directly
PyInstaller.__main__.run(['--distpath', distdir, '--workpath', builddir, win_spec])