#!/usr/bin/env python3

import os
import subprocess

"""
build HL7-strings
"""


class HL7Msg(object):

    def __init__(self):
        self.hl7_msg = None
        self.project_id = None
        self.subject_id = None
        self.subject_dob = None
        self.sex = None
        self.session_id = None
        self.study_date = None
        self.performing = None
        self.pi = None

        self.host = None
        self.port = None

        # dcm4che hl7-send path
        self.hl7snd = os.path.join(os.getcwd(), 'bin', 'dcm4che-5.16.3', 'bin', 'hl7snd')
        if os.name == "nt":
            self.hl7snd += '.bat'

    def setParameters(self, project_id="", subject_id="", subject_dob="", sex="", session_id="", study_date="",
                      performing=None, pi=None, host='127.0.0.1', port=65432, dosend=False):
        self.project_id = project_id
        self.subject_id = subject_id
        self.subject_dob = subject_dob
        self.sex = sex
        self.session_id = session_id
        self.study_date = study_date

        if performing is None:
            self.performing = "^^^"
        else:
            self.performing = "{title}^{lastname}^{surname}^".format(**performing)

        if pi is None:
            self.pi = "^^^"
        else:
            self.pi = "{title}^{lastname}^{surname}^".format(**pi)

        self.host = host
        self.port = port
        self.dosend = dosend

    def buildHL7String(self):
        import datetime
        base_HL7_msg = self.getBaseHL7Msg()

        send_date = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        # xnat_string = "Project:{0};Subject:{1};Session:pro-{0}_sub-{1}_ses-{2}".format(self.project_id, self.subject_id,
        #                                                                                self.session_id)
        accession_nr = "NIC-{}".format(send_date[2:-1])

        self.hl7_msg = base_HL7_msg.format(PROJECT_ID=self.project_id, SUBJECT_ID=self.subject_id,
                                           SESSION_ID=self.session_id, SUBJECT_DOB=self.subject_dob, SEX=self.sex,
                                           PERFORMING_PHYSICIAN=self.performing, PI=self.pi,
                                           SENDDATE=send_date, STUDYDATE=self.study_date, ACCESSIONNR=accession_nr)

        return self.hl7_msg

    def sendHL7(self):
        # import socket

        self.buildHL7String()

        sendfile = os.path.abspath("send.hl7")

        with open(sendfile, 'w', newline='\n') as f:
            f.write(self.hl7_msg)

        try:
            if self.dosend:
                result = subprocess.check_output(
                    "{hl7snd} -c {host}:{port} {sendfile}".format(hl7snd=self.hl7snd, host=self.host, port=self.port,
                                                                  sendfile=sendfile), shell=True)
            else:
                result = "File successfully written."
        except Exception as e:
            return repr(e)
        return repr(result)


    def getBaseHL7Msg(self):
        # check expected path
        expected = 'msg.HL7'
        if not os.path.exists(expected):
            with open(expected, 'w', newline='\n') as f:
                f.write("""MSH|^~\&|RES-XNAT|MRH4WL|EE|PACS|{SENDDATE}||ORM^O01||P|2.3
PID|||{PROJECT_ID}_{SUBJECT_ID}^^^9636RES|0|{PROJECT_ID}^{SUBJECT_ID}^||{SUBJECT_DOB}|{SEX}|||||||||
PV1||O|MR4_UKD||{SESSION_ID}|{ACCESSIONNR}
ORC|NW||{ACCESSIONNR}^||||||{SENDDATE}|||{PERFORMING_PHYSICIAN}||||||MRH4^MR
OBR|||{ACCESSIONNR}^|NIC_UKD^MR-Forschungsstudie^MR||||||||||||{PI}||||||{SENDDATE}||MR||||||||||||{STUDYDATE}""")

        with open(expected, 'r') as f:
            base_HL7_msg = "".join(f.readlines())

        return base_HL7_msg
