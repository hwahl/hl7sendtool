#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl

from PySide2.QtCore import QDate, QDateTime
from PySide2.QtWidgets import QMainWindow, QDialog, QAction, QMessageBox
from .main import Ui_MainWindow
from .prop_host import Ui_propHost

import sys
import os

from lib.hl7 import HL7Msg


class HL7STMain(QMainWindow):

    def __init__(self, parent=None):
        super(HL7STMain, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)


        if os.path.exists('host'):
            with open('host', 'r') as f:
                tmp = f.readline().rstrip().split(':')
                self.host = tmp[0]
                self.port = int(tmp[1])
        else:
            self.host = "127.0.0.1"
            self.port = 65432

        self.dosend = True
        self.debug = False

        self.HL7msg = HL7Msg()

        self.initUI()
        self.initBars()

    def initUI(self):
        self.ui.dt_dob.setDate(QDate(1990, 1, 1))
        self.ui.dt_studydate.setDateTime(QDateTime.currentDateTime().addSecs(3600))

        self.ui.cb_sex.addItem('M')
        self.ui.cb_sex.addItem('F')
        self.ui.cb_sex.addItem('O')
        self.ui.cb_sex.setCurrentIndex(self.ui.cb_sex.findText('O'))

        self.ui.txt_perfph_last.setText('Fritz')
        self.ui.txt_perfph_sur.setText('Pomm')

        self.ui.txt_pi_last.setText('Lustig')
        self.ui.txt_pi_sur.setText('Peter')
        self.ui.txt_pi_title.setText('Prof')

        self.ui.pb_send.clicked.connect(self.sendHL7)
        self.ui.pb_cancel.clicked.connect(self.close)

    def initBars(self):
        self.statusBar().showMessage('Ready')

        menubar = self.menuBar()

        exitAct = QAction('&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)

        showPars = QAction('Set Host', self)
        showPars.setStatusTip('set Host.')
        showPars.triggered.connect(self.openPropHost)

        fileMenu = menubar.addMenu('&Menu')
        fileMenu.addAction(showPars)
        fileMenu.addAction(exitAct)

    def openPropHost(self):
        propHost = HL7STProp(self)
        propHost.show()

    def sendHL7(self):
        parameters = dict(project_id=self.ui.txt_project.text(),
                          subject_id=self.ui.txt_subject.text(),
                          subject_dob=self.ui.dt_dob.date().toString("yyyyMMdd"),
                          sex=self.ui.cb_sex.currentText(),
                          session_id=self.ui.txt_session.text(),
                          study_date=self.ui.dt_studydate.dateTime().toString("yyyyMMddHHmm"),
                          performing=dict(title=self.ui.txt_perfph_title.text(),
                                          lastname=self.ui.txt_perfph_last.text(),
                                          surname=self.ui.txt_perfph_sur.text()),
                          pi=dict(title=self.ui.txt_pi_title.text(),
                                  lastname=self.ui.txt_pi_last.text(),
                                  surname=self.ui.txt_pi_sur.text()),
                          host=self.host, port=self.port, dosend=self.dosend)
        self.HL7msg.setParameters(**parameters)
        status = self.HL7msg.sendHL7()

        if self.debug:
            msgbox = QMessageBox()
            msgbox.setText(status)
            msgbox.exec_()

        if status:
            self.statusBar().showMessage('Successfully send!')
        else:
            self.statusBar().showMessage('Error! Send again?')

    def __del__(self):
        sys.stdout = sys.__stdout__


class HL7STProp(QDialog):

    def __init__(self, parent=None):
        super(HL7STProp, self).__init__(parent)
        self.parent = parent
        self.ui = Ui_propHost()
        self.ui.setupUi(self)

        self.host = self.parent.host
        self.port = self.parent.port

        self.ui.chb_reallysend.setChecked(self.parent.dosend)

        self.ui.txt_host.setText("{}".format(self.parent.host))
        self.ui.txt_port.setText("{}".format(self.parent.port))

        self.ui.txt_host.textChanged[str].connect(self.onChangedStr)
        self.ui.txt_port.textChanged[str].connect(self.onChangedInt)

        self.ui.uiButtonBox.accepted.connect(self.saveHost)


    def onChangedStr(self, host):
        self.host = host

    def onChangedInt(self, integer):
        self.port = int(integer)


    def saveHost(self):
        self.parent.host = self.host
        self.parent.port = self.port
        self.parent.dosend = self.ui.chb_reallysend.isChecked()
