# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\prop_host.ui',
# licensing of '.\prop_host.ui' applies.
#
# Created: Thu Mar 14 15:37:29 2019
#      by: pyside2-uic  running on PySide2 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_propHost(object):
    def setupUi(self, propHost):
        propHost.setObjectName("propHost")
        propHost.resize(279, 69)
        self.horizontalLayoutWidget = QtWidgets.QWidget(propHost)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 261, 64))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.lbl_PORT = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.lbl_PORT.setObjectName("lbl_PORT")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.lbl_PORT)
        self.txt_port = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.txt_port.setClearButtonEnabled(True)
        self.txt_port.setObjectName("txt_port")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.txt_port)
        self.lbl_HOST = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.lbl_HOST.setObjectName("lbl_HOST")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.lbl_HOST)
        self.txt_host = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.txt_host.setText("")
        self.txt_host.setClearButtonEnabled(True)
        self.txt_host.setObjectName("txt_host")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.txt_host)
        self.chb_reallysend = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        self.chb_reallysend.setObjectName("chb_reallysend")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.chb_reallysend)
        self.horizontalLayout.addLayout(self.formLayout)
        self.uiButtonBox = QtWidgets.QDialogButtonBox(self.horizontalLayoutWidget)
        self.uiButtonBox.setOrientation(QtCore.Qt.Vertical)
        self.uiButtonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.uiButtonBox.setObjectName("uiButtonBox")
        self.horizontalLayout.addWidget(self.uiButtonBox)

        self.retranslateUi(propHost)
        QtCore.QObject.connect(self.uiButtonBox, QtCore.SIGNAL("accepted()"), propHost.accept)
        QtCore.QObject.connect(self.uiButtonBox, QtCore.SIGNAL("rejected()"), propHost.reject)
        QtCore.QMetaObject.connectSlotsByName(propHost)
        propHost.setTabOrder(self.txt_host, self.txt_port)

    def retranslateUi(self, propHost):
        propHost.setWindowTitle(QtWidgets.QApplication.translate("propHost", "Properties Host", None, -1))
        self.lbl_PORT.setText(QtWidgets.QApplication.translate("propHost", "PORT", None, -1))
        self.lbl_HOST.setText(QtWidgets.QApplication.translate("propHost", "HOST", None, -1))
        self.chb_reallysend.setText(QtWidgets.QApplication.translate("propHost", "really send", None, -1))

