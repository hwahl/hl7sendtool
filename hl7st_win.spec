# -*- mode: python -*-

block_cipher = None


a = Analysis(['hl7st_gui.py'],
             pathex=['H:\\Projekte\\Forschungsscanner\\HL7SendTool'],
             binaries=[],
             datas=[ ('.\\gui', 'gui'), ('.\\bin', 'bin')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='HL7SendTool',
          debug=False,
          strip=False,
          upx=False,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='HL7SendTool')
