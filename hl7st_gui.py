#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl

from PySide2.QtWidgets import QApplication

from gui.gui import HL7STMain

import sys

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("Fusion")

    main = HL7STMain()
    main.show()

    sys.exit(app.exec_())